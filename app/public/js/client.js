const socket = io();

const acknowledlgements = (err) => {
  if (err) {
    return alert(err);
  }
  console.log("đã gửi tin nhắn thành công");
};

document.getElementById("form-messages").addEventListener("submit", (e) => {
  e.preventDefault();
  const messages = document.getElementById("input-messages").value;

  // gửi messages lên server
  socket.emit("send-messages-client-to-server", messages, acknowledlgements);
});

// nhận messages từ server
socket.on("send-messages-server-to-client", (content) => {
  document.getElementById("message-list").innerHTML += `
    <div class="message-item">
      <div class="message__row1">
        <p class="message__name">${content.username}</p>
        <p class="message__date">${content.time}</p>
      </div>
      <div class="message__row2">
        <p class="message__content">
        ${content.messages}
        </p>
      </div>
    </div>
  `;
});

// lấy cái chuổi ( ?room=FiFA04&username=Ronaldo ) xuống code
const queryString = location.search;
const info = Qs.parse(queryString, {
  ignoreQueryPrefix: true,
});

socket.emit("join-room-client-to-server", info);

socket.on("send-user-list-server-to-client", (userList) => {
  document.getElementById("user-list-by-room").innerHTML = userList
    .map(
      (info) => `
      <li class="app__item-user">${info.username}</li>
    `
    )
    .reduce((sumString, stringHtml) => (sumString += stringHtml), "");
});

// xử lý chia sẽ vị trí
document.getElementById("btn-share-location").addEventListener("click", () => {
  if (!navigator.geolocation) {
    return alert("trình duyệt không hổ trợ");
  }

  navigator.geolocation.getCurrentPosition((position) => {
    const location = {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
    };
    socket.emit("share-location-client-to-server", location);
  });
});

socket.on("share-location-server-to-client", ({ username, time, messages }) => {
  console.log("run");
  document.getElementById("message-list").innerHTML += `
    <div class="message-item" >
      <div class="message__row1">
        <p class="message__name">${username}</p>
        <p class="message__date">${time}</p>
      </div>
      <div class="message__row2">
        <p class="message__content">
          <a href="${messages}" target="_blank">
            ${username} location 
          </a>
        </p>
      </div>
    </div>
  `;
});
