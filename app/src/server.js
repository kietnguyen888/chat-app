const express = require("express");
const path = require("path");
const { createServer } = require("http");
const { Server } = require("socket.io");
const FilterBadWords = require("bad-words");
const {
  addUser,
  getListUserByRoom,
  getUserById,
  removeUser,
} = require("./models/user");
const { generateMessages } = require("./utils/generate-messages");

const app = express();
const pathPublicDirectory = path.join(__dirname, "../public");
// http://localhost:5000/ <==> public
app.use(express.static(pathPublicDirectory));

const httpServer = createServer(app);

const io = new Server(httpServer, {
  /* options */
});

io.on("connection", (socket) => {
  console.log("new client conect : ", socket.id);

  // xử lý join room
  socket.on("join-room-client-to-server", ({ room, username }) => {
    socket.join(room); // đưa client vào phòng

    // thêm user vào danh sách user
    const newUser = {
      id: socket.id,
      room,
      username,
    };
    
    addUser(newUser);

    /***
     * xử lý câu chào:
     * 1/ user vừa kết nối vào : "chào mừng bạn đến với cyberchat"
     * 2/ các user đã kết nối trước đó : "có 1 user mới vừa kết nối vào cyberchat"
     */
    socket.emit(
      "send-messages-server-to-client",
      generateMessages("ADMIN", "chào mừng bạn đến với " + room)
    );
    socket.broadcast
      .to(room)
      .emit(
        "send-messages-server-to-client",
        generateMessages("ADMIN", username + " vừa vào " + room)
      );

    // nhận messages từ client
    socket.on("send-messages-client-to-server", (messages, callback) => {
      // kiểm tra tin nhắn có hợp lệ hay ko ?
      const filterBadWords = new FilterBadWords();
      if (filterBadWords.isProfane(messages)) {
        return callback("messages có từ khóa không tốt");
      }

      // gửi messages về cho các client
      const { username } = getUserById(socket.id);
      io.to(room).emit(
        "send-messages-server-to-client",
        generateMessages(username, messages)
      );

      // xử lý gửi tin nhắn thành công
      callback();
    });

    // xử lý trả danh sách người theo phòng về client
    const userList = getListUserByRoom(room);
    io.to(room).emit("send-user-list-server-to-client", userList);

    // xử lý chia sẽ vị trí
    socket.on("share-location-client-to-server", ({ latitude, longitude }) => {
      const urlLocation = `https://www.google.com/maps?q=${latitude},${longitude}`;
      const { username } = getUserById(socket.id);
      io.to(room).emit(
        "share-location-server-to-client",
        generateMessages(username, urlLocation)
      );
    });
  });

  socket.on("disconnect", () => {
    removeUser(socket.id);
    console.log(`client ${socket.id} disconenct`);
  });
});

httpServer.listen(5000, () => {
  console.log(` app run on port 5000 `);
});
