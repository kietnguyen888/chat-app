const dateFormat = require("date-format");

const generateMessages = (username, messages) => ({
  username,
  messages,
  time: dateFormat("dd/MM/yyyy - hh:mm", new Date()),
});

module.exports = {
  generateMessages,
};
